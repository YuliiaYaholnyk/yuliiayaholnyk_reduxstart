

import 'package:reduxStart/dto/image_dto.dart';
import 'package:reduxStart/service/photo_service.dart';

class ImageRepository {
  ImageRepository._privateConstructor();

  static final ImageRepository _instance = ImageRepository._privateConstructor();

  static ImageRepository get instance => _instance;

  Future<List<ImageDto>> getPhotos() async {
    return await PhotoService.instance.getData();
  }
}
