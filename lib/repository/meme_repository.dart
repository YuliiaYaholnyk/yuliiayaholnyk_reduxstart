
import 'dart:typed_data';

import 'package:reduxStart/service/meme_service.dart';

class MemeRepository {
  // region [Initialize]
  static const String TAG = '[MemeRepository]';

  MemeRepository._privateConstructor();

  static final MemeRepository _instance = MemeRepository._privateConstructor();

  static MemeRepository get instance => _instance;

  Future<Uint8List> getMeme() async {
    return await MemeService.instance.getMeme();
  }
  // endregion
}