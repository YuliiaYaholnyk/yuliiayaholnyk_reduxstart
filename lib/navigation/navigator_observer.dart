import 'package:flutter/material.dart';

class CustomNavigatorObserver extends NavigatorObserver{

  Function onReplace;

  void setOnReplace(Function func){
    onReplace = func;
  }

  @override
  void didReplace({ Route<dynamic> newRoute, Route<dynamic> oldRoute }) {
    if(onReplace!=null){
      this.onReplace(newRoute, oldRoute);
    }
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
  }



}