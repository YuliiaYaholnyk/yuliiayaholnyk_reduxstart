import 'package:flutter/cupertino.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

import 'package:redux/redux.dart';
import 'package:reduxStart/navigation/navigation_selector.dart';
import 'file:///C:/Users/AppVesto/development/projects/reduxStart/reduxStart/lib/navigation/const.dart';
import 'package:reduxStart/store/appstate/app_state.dart';


class NavigationViewModel {
  final void Function(String route) navigateTo;

  NavigationViewModel({@required this.navigateTo});

  static NavigationViewModel fromStore(Store<AppState> store){
    return NavigationViewModel(navigateTo: NavigationSelector.navigateTo(store));
  }
}