

const String FIRST_PAGE_ROUTE = '/firs-page';
const String SECOND_PAGE_ROUTE = '/second-page';
const String THIRD_PAGE_ROUTE = '/third-page';
const String FOURTH_PAGE_ROUTE = '/fourth-page';

const String IMAGE_PAGE_ROUTE = '/image-page';
const String MEME_PAGE_ROUTE = '/meme-page';

const String COUNTER_PAGE_DIVIDE =  '/counter-page-divide';
const String COUNTER_PAGE_MULTIPLY = '/counter-page-multiply';