
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';

class NavigationSelector {


  static void Function(String routeName) navigateTo(Store<AppState> store) {
    return (String routeName) => store.dispatch(NavigateToAction.replace(routeName));

  }


}