
import 'package:flutter/material.dart';
import 'package:reduxStart/pages/counter_page_divide/counter_page_divide.dart';
import 'package:reduxStart/pages/counter_page_multiply/counter_page_multiply.dart';
import 'package:reduxStart/pages/image_page.dart';
import 'package:reduxStart/pages/meme_page/meme_page.dart';
import 'package:reduxStart/screens/fourth_page.dart';
import 'package:reduxStart/screens/second_page.dart';
import 'package:reduxStart/screens/third_page.dart';
import 'package:reduxStart/ui/pages/counter_page/first_page.dart';

import 'const.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';
  RouteHelper._privateConstructor();
  static final RouteHelper _instance = RouteHelper._privateConstructor();
  static RouteHelper get instance => _instance;
  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {

      case FIRST_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: CounterPage(),
        );
      case SECOND_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: SecondPage(),
        );
      case THIRD_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: ThirdPage(),
        );
      case FOURTH_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: FourthPage(),
        );
      case IMAGE_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: ImagePage(),
        );
      case MEME_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: MemePage(),
        );
      case COUNTER_PAGE_DIVIDE:
        return _defaultRoute(
          settings: settings,
          page: CounterPageDivide(),
        );
      case COUNTER_PAGE_MULTIPLY:
        return _defaultRoute(
          settings: settings,
          page: CounterPageMultiply(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: CounterPage(),
        );
    }
  }
  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}