import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxStart/dictionary/flutter_diectionary.dart';
import 'package:reduxStart/navigation/const.dart';
import 'package:reduxStart/res/string_const.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/widgets/drawer_view_model.dart';

import '../navigation/navigation_view_mode.dart';

Map<String, String> languages = {
  'English': 'en',
  'Русский': 'ru',
};

Map<String, String> reverseLanguages = {
  'en': 'English',
  'ru': 'Русский',
};

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  String themeValue = FlutterDictionary.instance.language.generalDictionary.lightTheme;
  String value = 'English';
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationViewModel>(
      converter: NavigationViewModel.fromStore,
      builder: (BuildContext context, NavigationViewModel viewModel) {
        return StoreConnector<AppState, DrawerViewModel>(
          converter: DrawerViewModel.fromStore,
          builder: (BuildContext context, DrawerViewModel drawerViewModel) {
            return Drawer(
              elevation: 5.0,
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: DropdownButton<String>(
                      value: reverseLanguages['${drawerViewModel.locale}'],
                      icon: Icon(Icons.language),
                      elevation: 5,
                      onChanged: (String newValue) {
                        print(languages[newValue]);
                        drawerViewModel.changeLanguage(languages[newValue]);
                        drawerViewModel.decrement();
                       // value = reverseLanguages['${drawerViewModel.locale}'];
                      },
                      items: <String>[
                        'English',
                        'Русский',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: DropdownButton<String>(
                      value: drawerViewModel.themeName,
                      icon: Icon(Icons.filter),
                      iconSize: 20.0,
                      elevation: 5,
                      onChanged: (String newValue) {
                       drawerViewModel.changeTheme(newValue);
                       drawerViewModel.increment();
                      },
                      items: <String>[
                       LIGHT_THEME,
                       DARCULA,
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),

                  Text(
                    FlutterDictionary.instance.language.generalDictionary.switchAppBar,
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Switch(
                    value: drawerViewModel.value,
                    onChanged: (_) {
                      drawerViewModel.changeAppBarColor();
                    },
                  ),
                  Container(

                    height: 100.0,
                    padding: const EdgeInsets.all(15),
                    child: RaisedButton(
                      elevation: 5.0,
                      color: drawerViewModel.currentData.primaryColor,
                      onPressed: () => viewModel.navigateTo(FIRST_PAGE_ROUTE),
                      child: Text(
                        FlutterDictionary.instance.language.generalDictionary.firstPage,
                        style: drawerViewModel.currentData.textTheme.button,
                      ),
                    ),
                  ),
                  Container(
                    height: 100.0,
                    padding: const EdgeInsets.all(15),
                    child: RaisedButton(
                      elevation: 5.0,
                      color: drawerViewModel.currentData.primaryColor,
                      onPressed: () => viewModel.navigateTo(COUNTER_PAGE_DIVIDE),
                      child: Text(
                        FlutterDictionary.instance.language.generalDictionary.secondPage,
                        style: drawerViewModel.currentData.textTheme.button,
                      ),
                    ),
                  ),
                  Container(
                    height: 100.0,
                    padding: const EdgeInsets.all(15),
                    child: RaisedButton(
                      elevation: 5.0,
                      color: drawerViewModel.currentData.primaryColor,
                      onPressed: () => viewModel.navigateTo(COUNTER_PAGE_MULTIPLY),
                      child: Text(
                        FlutterDictionary.instance.language.generalDictionary.thirdPage,
                        style: drawerViewModel.currentData.textTheme.button,
                      ),
                    ),
                  ),
//                  Container(
//                    height: 100.0,
//                    padding: const EdgeInsets.all(15),
//                    child: RaisedButton(
//                      elevation: 5.0,
//                      color: Colors.blueGrey,
//                      onPressed: () => viewModel.navigateTo(FOURTH_PAGE_ROUTE),
//                      child: Text(
//                        'Fourth page',
//                        style: TextStyle(color: Colors.white),
//                      ),
//                    ),
//                  ),
                  Container(
                    height: 100.0,
                    padding: const EdgeInsets.all(15),
                    child: RaisedButton(
                      elevation: 5.0,
                      color: drawerViewModel.currentData.primaryColor,
                      onPressed: () {
                        drawerViewModel.cleanCounter();
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        FlutterDictionary.instance.language.generalDictionary.cleanCounter,
                        style: drawerViewModel.currentData.textTheme.button,
                      ),
                    ),
                  ),
//                  Container(
////                    height: 100.0,
////                    padding: const EdgeInsets.all(15),
////                    child: RaisedButton(
////                      elevation: 5.0,
////                      color: Colors.blueGrey,
////                      onPressed: () => viewModel.navigateTo(MEME_PAGE_ROUTE),
////                      child: Text(
////                        'Meme generate',
////                        style: TextStyle(color: Colors.white),
////                      ),
////                    ),
////                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: IconButton(
                            icon: Icon(Icons.add),
                            onPressed: () {
                              drawerViewModel.increment();
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: IconButton(
                            icon: Icon(Icons.remove),
                            onPressed: () {
                              drawerViewModel.decrement();
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
