import 'package:flutter/material.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:reduxStart/store/color_state/color_selector.dart';
import 'package:reduxStart/store/counter_state/counter_selector.dart';
import 'package:reduxStart/store/counter_state/second_counter_selector.dart';
import 'package:reduxStart/store/language_state/language_selector.dart';
import 'package:reduxStart/store/theme_state/theme_selector.dart';

class DrawerViewModel {
  final void Function() cleanCounter;
  final void Function() increment;
  final void Function() decrement;
  final bool value;
  final void Function() changeAppBarColor;
  final String locale;
  final void Function(String locale) changeLanguage;
  final ThemeData currentData;
  final void Function(String theme) changeTheme;
  final String themeName;

  DrawerViewModel({
    this.cleanCounter,
    this.value,
    this.changeAppBarColor,
    this.decrement,
    this.increment,
    this.locale,
    this.changeLanguage,
    this.currentData,
    this.changeTheme,
    this.themeName,
  });

  static DrawerViewModel fromStore(Store<AppState> store) {
    return DrawerViewModel(
      cleanCounter: CounterSelector.cleanCounter(store),
      value: ColorSelector.getValue(store),
      changeAppBarColor: ColorSelector.changeColorAppBar(store),
      increment: CounterSelector.getIncrementFunction(store),
      decrement: SecondCounterSelector.getDecrementFunction(store),
      locale: LanguageSelector.getSelectedLocale(store),
      changeLanguage: LanguageSelector.changeLanguage(store),
      currentData: ThemeSelector.getSelectedTheme(store),
      changeTheme: ThemeSelector.changeTheme(store),
      themeName: ThemeSelector.getSelectedThemeName(store),
    );
  }
}
