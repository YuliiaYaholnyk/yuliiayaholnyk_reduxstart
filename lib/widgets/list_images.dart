import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxStart/pages/image_page_view_model.dart';

class ListImages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector(
      converter: ImagePageViewModel.fromStore,
      onInitialBuild: (ImagePageViewModel viewModel) => viewModel.getRequest(),
      builder: (BuildContext context, ImagePageViewModel viewModel) {
       return ListView.builder(
          itemCount: viewModel.urls.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: Image(
                image: NetworkImage(viewModel.urls[index]),
              ),
            );
          },
        );
      }
    );
  }
}
