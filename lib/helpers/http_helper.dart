import 'dart:convert';
import 'dart:typed_data';
import 'package:image/image.dart' as im;
import 'dart:ui';
import 'package:http/http.dart' as http;

enum HttpType {
  get,
  put,
  post,
  delete,
}

class HttpHelper {
  HttpHelper._privateConstructor();

  static final HttpHelper _instance = HttpHelper._privateConstructor();

  static HttpHelper get instance => _instance;

  Future<Uint8List> request(String fullUrl, HttpType type) async {
    http.Response response;
    if (type == HttpType.get) {
      Map<String, String> headers = {
        'x-rapidapi-host': 'ronreiter-meme-generator.p.rapidapi.com',
        'x-rapidapi-key': '7337ac38d7msh18d3065017c1c0ep1f21c3jsn793bd2ae700d',
      };
      response = await http.get(
          'https://ronreiter-meme-generator.p.rapidapi.com/meme?font=Impact&font_size=30&meme=Condescending-Wonka&top=your face&bottom=when you want continue this task',
          headers: headers);
      print('RESPONSE HTTP HELPER');

//      final bytes64 = base64Encode(response.bodyBytes);
//      print('64');
//      print(bytes64);
      //Image image = base64


//      Image image = decodeImage(response.);
//      print(image.getBytes());
//      final path = await _localPath;
//      print('PATH  $path');

      // File('$path/test1.jpg').writeAsBytesSync(image.getBytes());
      return response.bodyBytes;

      //print(response.bodyBytes);
//    }
//    if (response != null && json.decode(response.body) != null) {
//      return json.decode(response.body) as List<dynamic>;
//    }
    }
  }
}

//Future<String> get _localPath async {
//  final directory = await getApplicationDocumentsDirectory();
//
//  return directory.path;
//}
