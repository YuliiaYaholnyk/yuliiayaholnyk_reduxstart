import 'package:flutter/material.dart';
import 'package:reduxStart/widgets/drawer.dart';

class MainLayout extends StatelessWidget {
  final Widget appBar;
  final Widget body;
  final Color bgColor;

  const MainLayout({
    Key key,
    this.appBar,
    this.body,
    this.bgColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      appBar: appBar,
      drawer: CustomDrawer(),
      body: body,
    );
  }
}
