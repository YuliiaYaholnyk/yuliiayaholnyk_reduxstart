import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reduxStart/navigation/const.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:reduxStart/store/color_state/color_selector.dart';

class AppBarLayout extends StatelessWidget with PreferredSizeWidget {
  final Text title;
  final String routePage;

  AppBarLayout({
    Key key,
    this.title,
    @required this.routePage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: false);
    return StoreConnector<AppState, AppBarViewModel>(
        converter: AppBarViewModel.fromStore,
        builder: (BuildContext context, AppBarViewModel viewModel) {
          return AppBar(
            centerTitle: true,
            title: title,
            backgroundColor: viewModel.color,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon:  Icon(
                    Icons.arrow_forward_ios,
                    size: preferredSize.height *0.5,
                  ),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            ),
          );
        });
  }

  double appBarHeight() {
    if (routePage == FIRST_PAGE_ROUTE) return 60.0;
    if (routePage == SECOND_PAGE_ROUTE) return 70.0;
    return 60.0;
  }

  @override
  Size get preferredSize => Size(ScreenUtil.screenWidth, appBarHeight().h);
}

class AppBarViewModel {
  final Color color;

  AppBarViewModel({this.color});

  static AppBarViewModel fromStore(Store<AppState> store) {
    return AppBarViewModel(
      color: ColorSelector.getColor(store),
    );
  }
}
