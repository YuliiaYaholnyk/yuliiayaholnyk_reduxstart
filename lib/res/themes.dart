

import 'package:flutter/material.dart';
import 'package:reduxStart/dictionary/flutter_diectionary.dart';
import 'package:reduxStart/res/string_const.dart';

Map <String, ThemeData> themes = {
  LIGHT_THEME : ThemeData(
    primaryColor: Colors.yellow,
    accentColor: Colors.blue,
    textTheme: TextTheme(
      button: TextStyle(color: Colors.black)
    )
  ),
  DARCULA: ThemeData(
    primaryColor: Colors.blueGrey,
    accentColor: Colors.blue,
      textTheme: TextTheme(
          button: TextStyle(color: Colors.white)
      )
  ),


};