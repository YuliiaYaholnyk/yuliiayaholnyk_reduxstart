import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/photo_state/photo_epics.dart';
import 'package:reduxStart/store/photo_state/photo_state.dart';
import 'package:redux_epics/redux_epics.dart';

import 'application/application.dart';

void main() {

  final store = new Store<AppState>(
    AppState.reducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
      EpicMiddleware(AppState.getAppEpics),
    ],
  );


  runApp(MyApp(store));
}

