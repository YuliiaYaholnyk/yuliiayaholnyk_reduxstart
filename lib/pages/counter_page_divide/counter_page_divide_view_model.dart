import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/counter_selector.dart';
import 'package:reduxStart/store/counter_state/second_counter_selector.dart';

class CounterPageDivideViewModel {
  final int secondCounter;

  CounterPageDivideViewModel({this.secondCounter});

  static CounterPageDivideViewModel fromStore(Store<AppState> store) {
    return CounterPageDivideViewModel(
      secondCounter: SecondCounterSelector.getCurrentCounter(store),
    );
  }
}
