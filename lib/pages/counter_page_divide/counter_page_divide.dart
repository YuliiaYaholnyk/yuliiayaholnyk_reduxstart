import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxStart/layout/app_bar_layout.dart';
import 'package:reduxStart/layout/main_layout.dart';
import 'package:reduxStart/store/appstate/app_state.dart';

import 'counter_page_divide_view_model.dart';

class CounterPageDivide extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBarLayout(
        title: Text('Counter  2'),
      ),
      body: StoreConnector<AppState, CounterPageDivideViewModel>(
        converter: CounterPageDivideViewModel.fromStore,
        builder: (BuildContext context, CounterPageDivideViewModel viewModel) {
          return Center(
            child: Text(
              "${viewModel.secondCounter}",
              style: TextStyle(
                fontSize: 100.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          );
        }
      ),
    );
  }
}
