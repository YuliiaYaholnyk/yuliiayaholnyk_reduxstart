

import 'package:flutter/material.dart';
import 'package:reduxStart/layout/main_layout.dart';
import 'package:reduxStart/widgets/list_images.dart';

class ImagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('Image Page'),
      ),
      body: ListImages(),
    );
  }
}



