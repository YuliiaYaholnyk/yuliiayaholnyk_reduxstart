import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxStart/layout/app_bar_layout.dart';
import 'package:reduxStart/layout/main_layout.dart';
import 'package:reduxStart/store/appstate/app_state.dart';

import 'counter_page_multiply_view_model.dart';

class CounterPageMultiply extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      bgColor: Colors.brown,
      appBar: AppBarLayout(
        title: Text('Counter * 10'),
      ),
      body: StoreConnector<AppState, CounterPageMultiplyViewModel>(
          converter: CounterPageMultiplyViewModel.fromStore,
          builder: (BuildContext context, CounterPageMultiplyViewModel viewModel) {
            return Center(
              child: Text(
                '${viewModel.counterMultiply}',
                style: TextStyle(
                  fontSize: 100.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            );
          }),
    );
  }
}
