import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/counter_selector.dart';

class CounterPageMultiplyViewModel {
  final double counterMultiply;

  CounterPageMultiplyViewModel({
    this.counterMultiply,
  });

  static CounterPageMultiplyViewModel fromStore(Store<AppState> store) {
    return CounterPageMultiplyViewModel(
      counterMultiply: CounterSelector.getCounterMultiplyByTen(store),
    );
  }
}
