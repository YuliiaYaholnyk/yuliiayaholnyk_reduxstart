import 'dart:typed_data';

import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/meme_state/meme_selector.dart';

class MemePageViewModel {
  final void Function() getMeme;
  final Uint8List mem;

  MemePageViewModel({this.getMeme, this.mem});

  static MemePageViewModel fromStore(Store<AppState> store) {
    return MemePageViewModel(
      getMeme: MemeSelector.getMeme(store),
      mem: MemeSelector.get(store),
    );
  }
}
