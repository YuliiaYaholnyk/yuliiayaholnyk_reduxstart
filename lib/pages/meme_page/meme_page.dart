import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';

import 'meme_page_view_model.dart';

class MemePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meme generator'),
      ),
      body: StoreConnector<AppState, MemePageViewModel>(
          converter: MemePageViewModel.fromStore,
          onInitialBuild: (MemePageViewModel viewModel) => viewModel.getMeme(),
          builder: (BuildContext context, MemePageViewModel viewModel) {
            return Image.memory(viewModel.mem);
          }),
    );
  }
}
