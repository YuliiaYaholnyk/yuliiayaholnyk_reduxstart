
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:reduxStart/store/photo_state/photo_selector.dart';




class ImagePageViewModel {
  final List<String> urls;
  void Function() getRequest;


  ImagePageViewModel({this.urls, this.getRequest});

  static ImagePageViewModel fromStore(Store <AppState> store){
    print('VM');
    return ImagePageViewModel(
      urls: PhotoSelector.getUrls(store),
      getRequest: PhotoSelector.getRequest(store),
    );
  }



}