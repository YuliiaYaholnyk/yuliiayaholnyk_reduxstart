
import 'package:flutter/foundation.dart';

import 'image_urls_dto.dart';

class ImageDto {
  final String id;
  final ImageUrlsDto imageUrls;


  ImageDto({
    @required this.id,
    @required this.imageUrls,

  });

  factory ImageDto.fromJson(Map<String, dynamic> json) {
    return ImageDto(
      id: json["id"],
      imageUrls: ImageUrlsDto.fromJson(json["urls"]),
    );
  }

}
