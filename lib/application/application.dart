import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';

import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:reduxStart/dictionary/flutter_diectionary.dart';
import 'package:reduxStart/navigation/navigator_observer.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/widgets/drawer_view_model.dart';

import '../navigation/route_helper.dart';
import 'package:flutter/widgets.dart';

class MyApp extends StatelessWidget {

  final Store store;

  MyApp(this.store);

  @override
  Widget build(BuildContext context) {
    CustomNavigatorObserver navigatorObserver = CustomNavigatorObserver();
    return StoreProvider<AppState>(
      store: store,
      child: StoreConnector<AppState, DrawerViewModel>(
        converter: DrawerViewModel.fromStore,
        builder: (BuildContext context, DrawerViewModel viewModel) {
          return MaterialApp(
            title: 'Flutter Demo',

            onGenerateTitle: (BuildContext context) {
              FlutterDictionary.init(context);
              return FlutterDictionary
                  .instance.language.generalDictionary.secondPage;
            },
            localizationsDelegates: [
              const FlutterDictionaryDelegate(),
              GlobalWidgetsLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            locale: Locale(viewModel.locale),
            supportedLocales:
            FlutterDictionaryDelegate.supportedLanguages.map<Locale>(
                  (supportedLocales) => Locale(supportedLocales.locale),
            ),
            navigatorKey: NavigatorHolder.navigatorKey,
            navigatorObservers: [
              navigatorObserver
            ],
            onGenerateRoute: (RouteSettings settings) => RouteHelper.instance.onGenerateRoute(settings),
            theme: viewModel.currentData,
          );
        }
      ),
    );
  }
}