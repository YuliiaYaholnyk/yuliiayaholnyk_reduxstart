
import 'package:flutter/cupertino.dart';

import '../language.dart';


class SupportedLocale {
  String locale;
  Language language;

  SupportedLocale({
    @required this.language,
    @required this.locale,
  });
}
