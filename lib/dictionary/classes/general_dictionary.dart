import 'package:flutter/foundation.dart';

class GeneralDictionary {
  final String switchAppBar;
  final String lightTheme;
  final String darcula;
  final String firstPage;
  final String secondPage;
  final String thirdPage;
  final String cleanCounter;
  final String loading;

  GeneralDictionary({
    @required this.switchAppBar,
    @required this.lightTheme,
    @required this.darcula,
    @required this.firstPage,
    @required this.secondPage,
    @required this.thirdPage,
    @required this.cleanCounter,
    @required this.loading,
  });


}
