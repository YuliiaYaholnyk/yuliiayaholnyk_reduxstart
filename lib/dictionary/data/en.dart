import 'package:reduxStart/dictionary/classes/general_dictionary.dart';
import 'package:reduxStart/res/string_const.dart';

import '../language.dart';

final Language en = Language(
  generalDictionary: GeneralDictionary(
    switchAppBar: 'Switch app bar color',
    lightTheme: LIGHT_THEME,
    darcula: DARCULA,
    firstPage: 'First page',
    secondPage: 'Second page',
    thirdPage: 'Third page',
    cleanCounter: 'Clean counter',
    loading: 'Loading...',
  ),
);
