import 'package:reduxStart/dictionary/classes/general_dictionary.dart';

import '../language.dart';

final Language ru = Language(
  generalDictionary: GeneralDictionary(
    switchAppBar: 'Изменить цвет заголовка',
    lightTheme: 'Светлая тема',
    darcula: 'Темная тема',
    firstPage: 'Первая страница',
    secondPage: 'Вторая страница',
    thirdPage: 'Третья страница',
    cleanCounter: 'Очистить счетчик',
    loading: 'Загрузка...',
  ),
);
