
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';
import 'package:reduxStart/dictionary/data/ru.dart';

import 'classes/supported_locale.dart';
import 'data/en.dart';
import 'language.dart';

class FlutterDictionary {
  Locale locale;
  Language language;

  static FlutterDictionary instance;

  FlutterDictionary(this.locale) {
    setNewLanguage(locale.languageCode);
  }

  static FlutterDictionary of(BuildContext context) {
    return Localizations.of<FlutterDictionary>(context, FlutterDictionary);
  }

  static void init(BuildContext context) {
    instance = FlutterDictionary.of(context);
  }

  //region [RTL LANGUAGES]
  static const List<String> _rtlLanguages = <String>[
    'ar', // Arabic
    'fa', // Farsi
    'he', // Hebrew
    'ps', // Pashto
    'ur', // Urdu
  ];

  void setNewLanguage(String locale) {
    print('Set new Language');
    print(locale);
    FlutterDictionaryDelegate.supportedLanguages.forEach((supportedLanguage) {
      if (supportedLanguage.locale == locale) {
        language = supportedLanguage.language;
        this.locale = Locale(locale);
      }
    });
  }

  bool isRTL(BuildContext context) {
    return _rtlLanguages.contains(
      Localizations.localeOf(context).languageCode,
    );
  }
//endregion
}

class FlutterDictionaryDelegate
    extends LocalizationsDelegate<FlutterDictionary> {
  const FlutterDictionaryDelegate();

  static List<SupportedLocale> supportedLanguages = [
    SupportedLocale(locale: 'en', language: en),
    SupportedLocale(locale: 'ru', language: ru),
  ];

  ///locales added here are supported by the dictionary, but not yet by the app
  @override
  bool isSupported(Locale locale) => supportedLanguages.any(
          (supportedLanguage) => supportedLanguage.locale == locale.languageCode);

  @override
  Future<FlutterDictionary> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<FlutterDictionary>(FlutterDictionary(locale));
  }

  @override
  bool shouldReload(LocalizationsDelegate<FlutterDictionary> old) {
    return false;
  }
}
