import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxStart/layout/app_bar_layout.dart';
import 'package:reduxStart/layout/main_layout.dart';
import 'package:reduxStart/navigation/const.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'counter_page_viewmidel.dart';

class CounterPage extends StatelessWidget {
  final int counter = 0;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: true);
    return MainLayout(
      appBar: AppBarLayout(
        title: Text('Counter Page'),
        routePage: FIRST_PAGE_ROUTE,
      ),
      body: FirstPage(),
    );
  }
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
      builder: (BuildContext context, CounterPageViewModel viewModel) => Container(
        child: Center(
          child: Text(
            viewModel.counter.toString(),
            style: TextStyle(
              fontSize: 100.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
