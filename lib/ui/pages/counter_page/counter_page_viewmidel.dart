import 'package:redux/redux.dart';
import 'package:flutter/foundation.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/counter_selector.dart';


class CounterPageViewModel {
  final int counter;
  final void Function() increment;
  final void Function() decrement;

  CounterPageViewModel({@required this.counter,@required this.increment,@required this.decrement,});

  static CounterPageViewModel fromStore(Store<AppState> store){

    return CounterPageViewModel(
      counter: CounterSelector.getCurrentCounter(store),
      increment: CounterSelector.getIncrementFunction(store) ,
      decrement: CounterSelector.getDecrementFunction(store),
    );
  }



}