import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reduxStart/layout/app_bar_layout.dart';
import 'package:reduxStart/layout/main_layout.dart';
import 'package:reduxStart/navigation/const.dart';

var list = List.generate(100, (index) => index);

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  bool ascending = true;

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      body: SecondPageView(),
      appBar: AppBarLayout(
        title: Text('SecondPage'),
        routePage: SECOND_PAGE_ROUTE,
      ),
    );
  }
}

class SecondPageView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: Container(
            height: ScreenUtil.screenHeight / 2,
            width: ScreenUtil.screenWidth / 2,
            child: CustomPaint(
              painter: CirclePaint(),
            ),
          ),
        ),
        ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: list.length,
            itemBuilder: (BuildContext context, int i) {
              return Card(
                elevation: 5.0,
                color: Colors.black45,
                margin: EdgeInsets.all(10.0),
                child: Text(
                  list[i].toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 100.h,
                  ),
                ),
              );
            }),
      ],
    );
  }
}

class CirclePaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint line = new Paint()
      ..color = Colors.black87
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line2 = new Paint()
      ..color = Colors.green
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line3 = new Paint()
      ..color = Colors.red
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line4 = new Paint()
      ..color = Colors.yellow
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line5 = new Paint()
      ..color = Colors.purple
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint complete = new Paint()
      ..color = Colors.red
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10;
    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    canvas.drawCircle(center, radius, line);
    double arcAngle = 2 * pi * (20 / 100);
    double arcAngle2 = 2 * pi * (-10 / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2.5, 1, false, line2);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 6, 1, false, line3);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / -7, 1.1, false, line4);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / -2, 1.6, false, line5);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 1.5, 1.1, false, line);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 1, 1.1, false, complete);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
