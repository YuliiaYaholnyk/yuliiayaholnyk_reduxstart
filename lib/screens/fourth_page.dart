import 'package:flutter/material.dart';
import 'package:reduxStart/layout/app_bar_layout.dart';
import 'package:reduxStart/layout/main_layout.dart';
import 'package:reduxStart/widgets/drawer.dart';

import 'color_filtred_widget.dart';

class FourthPage extends StatefulWidget {
  @override
  _FourthPageState createState() => _FourthPageState();
}

class _FourthPageState extends State<FourthPage> {
  PageController controller;

  @override
  void initState() {
    controller = PageController(
      initialPage: 1,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBarLayout(title: Text('FourthPage'),),
      body:PageView(
        controller: controller,
        scrollDirection: Axis.vertical,
        children: <Widget>[
          ColorFiltredWidget(),
          ColorFiltredWidget(),
          ColorFiltredWidget(),
        ],
      ),
    );
  }
}

