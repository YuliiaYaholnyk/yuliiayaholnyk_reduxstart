
import 'dart:typed_data';

import 'package:reduxStart/const/url_const.dart';
import 'package:reduxStart/helpers/http_helper.dart';

class MemeService {
  MemeService._privateConstructor();

  static final MemeService _instance = MemeService._privateConstructor();

  static MemeService get instance => _instance;
  Future<Uint8List> getMeme() async {
    try {
      Uint8List response =
      await HttpHelper.instance.request(MEME_URL, HttpType.get);
      return response;
      print('--------RESPONSE---------');
      print(response);
    } catch (error) {
      print(error);
    }

  }
}