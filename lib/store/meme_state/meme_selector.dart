import 'dart:typed_data';

import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:reduxStart/store/meme_state/meme_actions.dart';
class MemeSelector {


  static void Function() getMeme(Store<AppState> store) {
    return () => store.dispatch(MemeActionRequest());
  }
  static Uint8List get(Store<AppState> store) {
    return  store.state.memeState.image;
  }

}