import 'dart:typed_data';

import 'package:image/image.dart';

class MemeActionRequest {
  MemeActionRequest();
}

class SetMemeAction {
  final Uint8List image;

  SetMemeAction({
    this.image,
  });
}

class SaveMemeAction{
final Uint8List image;

SaveMemeAction({
  this.image,
});

}
