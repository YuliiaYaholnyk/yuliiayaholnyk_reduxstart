
import 'dart:typed_data';


import 'package:reduxStart/repository/meme_repository.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import 'meme_actions.dart';
class MemeEpics {
  static final indexEpic = combineEpics<AppState>([
    requestMemeEpic,
   setInitMemeDataEpic,
  ]);
  static Stream<dynamic> requestMemeEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<MemeActionRequest>().switchMap((action) {
      return Stream.fromFuture(
        MemeRepository.instance.getMeme().then((Uint8List image) {
          if (image == null) {
            print('------------NOT IMAGE--------');
            return EmptyAction();
          }
          return SetMemeAction(
            image: image,
          );
        }),
      );
    });
  }
  static Stream<dynamic> setInitMemeDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<SetMemeAction>().switchMap((action) {
      return Stream.fromIterable([
        SaveMemeAction(image: action.image),
        //NavigateToAction.pushNamedAndRemoveUntil(ROUTE_HOME_PAGE, (route) => false)
      ]);
    });
  }
}
