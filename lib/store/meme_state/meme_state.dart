import 'dart:collection';
import 'dart:typed_data';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'dart:ui';

import 'meme_actions.dart';
class MemeState {
 final Uint8List image;
  MemeState({this.image});

  factory MemeState.initial() => MemeState(
    image: Uint8List(0),
  );

  MemeState reducer(dynamic action) {
    print(action.runtimeType);
    return Reducer<MemeState>(
      actions: HashMap.from({
        SaveMemeAction : (dynamic action)=> _convertToUi((action as SaveMemeAction).image),
      }),
    ).updateState(action, this);
  }

 MemeState _convertToUi(Uint8List image){
   // ui.Image im = decodeImage(image.getBytes());
  return MemeState(
    image: image
  );
  }
}
