
import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';

import 'language_actions.dart';

class LanguageSelector {
  static String getSelectedLocale(Store<AppState> store) {
    return store.state.languageState.locale;
  }

  static void Function(String) changeLanguage(Store<AppState> store) {
    return (String locale) {
      store.dispatch(
        ChangeLanguageAction(locale: locale),
      );
    };
  }
}
