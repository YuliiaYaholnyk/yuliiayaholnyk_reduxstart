

import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:reduxStart/dictionary/flutter_diectionary.dart';
import 'package:reduxStart/store/appstate/app_state.dart';

import 'language_actions.dart';

class LanguageState {
  final String locale;

  LanguageState({
    @required this.locale,
  });

  factory LanguageState.initial() {
    return LanguageState(
      locale: 'en',
    );
  }

  LanguageState copyWith({
    @required String locale,
  }) {
    return LanguageState(
      locale: locale ?? this.locale,
    );
  }

  LanguageState reducer(dynamic action) {
    return Reducer<LanguageState>(
      actions: HashMap.from({
        ChangeLanguageAction: (dynamic action) =>
            changeLanguageFunction(action as ChangeLanguageAction),
      }),
    ).updateState(action, this);
  }

  LanguageState changeLanguageFunction(ChangeLanguageAction action) {
    print('Change lang func');
    print(action.locale);
    FlutterDictionary.instance.setNewLanguage(action.locale);
    return this.copyWith(
      locale: action.locale,
    );
  }
}
