import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:reduxStart/dictionary/flutter_diectionary.dart';
import 'package:reduxStart/res/string_const.dart';
import 'package:reduxStart/res/themes.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';
import 'package:reduxStart/store/theme_state/theme_actions.dart';

class ThemeState {
  static const String TAG = 'Theme state';
  final ThemeData theme;
  final String themeName;

  ThemeState({
    this.theme,
    this.themeName,
  });

  factory ThemeState.initial() => ThemeState(
        theme: themes[LIGHT_THEME],
        themeName: LIGHT_THEME,
      );

  ThemeState reducer(dynamic action) {
    return Reducer<ThemeState>(
      actions: HashMap.from({
        ChangeTheme: (dynamic action) => _changeTheme((action as ChangeTheme).theme),
      }),
    ).updateState(action, this);
  }

  ThemeState _changeTheme(String nameTheme) {
    return ThemeState(
      theme: themes[nameTheme],
      themeName: nameTheme,
    );
  }
}
