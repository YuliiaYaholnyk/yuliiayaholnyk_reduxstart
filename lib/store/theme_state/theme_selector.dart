


import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/theme_state/theme_actions.dart';

class ThemeSelector {
  static ThemeData getSelectedTheme(Store<AppState> store) {
    return store.state.themeState.theme;
  }

  static String getSelectedThemeName(Store<AppState> store) {
    return store.state.themeState.themeName;
  }

  static void Function(String) changeTheme(Store<AppState> store) {
    return (String theme) {
      store.dispatch(
        ChangeTheme(theme: theme),
      );
    };
  }
}
