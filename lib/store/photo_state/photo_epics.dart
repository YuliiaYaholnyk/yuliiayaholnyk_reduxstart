
import 'package:reduxStart/dto/image_dto.dart';
import 'package:reduxStart/repository/photo_repository.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class PhotoEpics {
  static final indexEpic = combineEpics<AppState>([
    requestEpic,
    setInitDataEpic,
  ]);
  static Stream<dynamic> requestEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    print('EPIC');
    return actions.whereType<RequestAction>().switchMap((action) {
      return Stream.fromFuture(
       ImageRepository.instance.getPhotos().then((List<ImageDto> imageDto) {
          if (imageDto == null) {
            return EmptyAction();
          }
          return SetInitDataAction(
            data: imageDto,
          );
        }),
      );
    });
  }
  static Stream<dynamic> setInitDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    print('setInitDataEpic');
    return actions.whereType<SetInitDataAction>().switchMap((action) {
      return Stream.fromIterable([
        SaveDataAction(data: action.data),
        //NavigateToAction.pushNamedAndRemoveUntil(ROUTE_HOME_PAGE, (route) => false)
      ]);
    });
  }
}


