import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';

class PhotoSelector {


  static void Function() getRequest(Store<AppState> store) {
    return () => store.dispatch(RequestAction());
  }

  static List<String> getUrls(Store<AppState> store) {
    return  store.state.photoState.imageUrls;
  }
}