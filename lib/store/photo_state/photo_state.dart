import 'dart:collection';

import 'package:reduxStart/dto/image_dto.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';


class PhotoState {
  static const String TAG = 'PhotoState';
  final List<String> imageUrls;

  PhotoState (this.imageUrls);

  factory PhotoState.initial() => PhotoState (List());


  PhotoState reducer(dynamic action) {
    print('$TAG => Action runtimetype => ${action.runtimeType}');
    return Reducer<PhotoState>(
      actions: HashMap.from({
        SaveDataAction: (dynamic action) => saveData((action as SaveDataAction).data),
      }),
    ).updateState(action, this);
  }

  PhotoState saveData(List<ImageDto> data){
    List<String> urls = [];
    data.forEach((el) {
     urls.add( el.imageUrls.regular);
    });
    return PhotoState(urls);
  }

}