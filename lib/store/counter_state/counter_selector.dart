import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';

class CounterSelector {
  static void Function() getIncrementFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() getDecrementFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }

  static int getCurrentCounter(Store<AppState> store) {
    return store.state.counterState.counter;
  }

  static double getCounterDivideByTwo(Store<AppState> store) {
    return store.state.counterState.counter / 2.0;
  }

  static double getCounterMultiplyByTen(Store<AppState> store) {
    return store.state.counterState.counter * 10.0;
  }

  static void Function() cleanCounter(Store<AppState> store) {
    return () => store.dispatch(CleanCounterAction());
  }
}
