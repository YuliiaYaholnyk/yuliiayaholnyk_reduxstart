import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';

class SecondCounterSelector {


  static void Function() getDecrementFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }

  static int getCurrentCounter(Store<AppState> store) {
    return store.state.secondCounterState.counter;
  }

}
