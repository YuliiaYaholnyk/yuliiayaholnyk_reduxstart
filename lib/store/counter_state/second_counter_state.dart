import 'dart:collection';

import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';

class SecondCounterState {
  static const String TAG = 'CounterState';
  final int counter;

  SecondCounterState(this.counter);

  factory SecondCounterState.initial() => SecondCounterState(0);

  SecondCounterState reducer(dynamic action) {
    return Reducer<SecondCounterState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => increment(),
        DecrementAction: (dynamic action) => decrement(),
        CleanCounterAction: (dynamic action) => cleanCounter(),
      }),
    ).updateState(action, this);
  }

  SecondCounterState cleanCounter() {
    return SecondCounterState(0);
  }

  SecondCounterState increment() {
    return SecondCounterState(this.counter + 1);
  }

  SecondCounterState decrement() {
    return SecondCounterState(this.counter - 1);
  }
}
