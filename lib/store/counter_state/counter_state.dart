import 'dart:collection';

import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';


class CounterState {
  static const String TAG = 'CounterState';
  final int counter;

  CounterState (this.counter);

  factory CounterState .initial() => CounterState (0);


  CounterState reducer(dynamic action) {
   // print('$TAG => Action runtimetype => ${action.runtimeType}');
    return Reducer<CounterState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => increment(),
       // DecrementAction: (dynamic action) => decrement(),
        CleanCounterAction:(dynamic action) => cleanCounter(),
      }),
    ).updateState(action, this);
  }

CounterState cleanCounter(){
    return CounterState(0);
}

  CounterState increment() {
    return CounterState(this.counter+1);
  }

  CounterState decrement() {
    return CounterState(this.counter - 1);
  }
}