import 'package:flutter/material.dart';
import 'package:reduxStart/dto/image_dto.dart';
import 'package:reduxStart/models/model.dart';

class IncrementAction {
  IncrementAction();
}

class ChangeColorAppBar{
  ChangeColorAppBar();
}

class DecrementAction {
  DecrementAction();
}

class CleanCounterAction {
  CleanCounterAction();
}

class RequestAction {
  RequestAction();
}

class SaveDataAction {
  final List<ImageDto> data;

  SaveDataAction({this.data});
}

class SetInitDataAction {
  final List<ImageDto> data;

  SetInitDataAction({
    this.data,
  });
}

class EmptyAction {
  EmptyAction();
}
