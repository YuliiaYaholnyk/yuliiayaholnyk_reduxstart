import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:reduxStart/store/color_state/color_state.dart';
import 'package:reduxStart/store/counter_state/counter_state.dart';
import 'package:reduxStart/store/counter_state/second_counter_state.dart';
import 'package:reduxStart/store/language_state/language_state.dart';
import 'package:reduxStart/store/meme_state/meme_epics.dart';
import 'package:reduxStart/store/meme_state/meme_state.dart';
import 'package:reduxStart/store/photo_state/photo_epics.dart';
import 'package:reduxStart/store/photo_state/photo_state.dart';
import 'package:reduxStart/store/theme_state/theme_state.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final LanguageState languageState;
  final CounterState counterState;
  final PhotoState photoState;
  final ColorState colorState;
  final MemeState memeState;
  final ThemeState themeState;
  final SecondCounterState secondCounterState;

  AppState({
    @required this.counterState,
    @required this.photoState,
    @required this.colorState,
    @required this.memeState,
    @required this.languageState,
    @required this.themeState,
    @required this.secondCounterState,
  });

  static final getAppEpics = combineEpics<AppState>([
    PhotoEpics.indexEpic,
    MemeEpics.indexEpic,
  ]);

  factory AppState.initial() => AppState(
        counterState: CounterState.initial(),
        photoState: PhotoState.initial(),
        colorState: ColorState.initial(),
        memeState: MemeState.initial(),
        languageState: LanguageState.initial(),
        themeState: ThemeState.initial(),
        secondCounterState: SecondCounterState.initial(),
      );

  static AppState reducer(AppState state, dynamic action) {
    return AppState(
      counterState: state.counterState.reducer(action),
      photoState: state.photoState.reducer(action),
      colorState: state.colorState.reducer(action),
      memeState: state.memeState.reducer(action),
      languageState: state.languageState.reducer(action),
      themeState: state.themeState.reducer(action),
      secondCounterState: state.secondCounterState.reducer(action),
    );
  }
}

class Reducer<T> {
  final String TAG = '[Reducer<$T>]';
  HashMap<dynamic, T Function(dynamic)> actions;

  Reducer({
    @required this.actions,
  }) {
    actions.forEach((key, value) {
      if (value == null) throw ('All Functions must be initialize');
    });
  }

  T updateState(dynamic action, T state) {
    if (actions.containsKey(action.runtimeType)) {
      return actions[action.runtimeType](action);
    }
    return state;
  }
}
