import 'dart:collection';
import 'package:flutter/material.dart';

import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';

class ColorState {
  final Color appBarColor;
  final bool value;

  ColorState({this.appBarColor, this.value});

  factory ColorState.initial() => ColorState(
        appBarColor: Colors.blue,
        value: false,
      );

  ColorState reducer(dynamic action) {
    print(action.runtimeType);
    return Reducer<ColorState>(
      actions: HashMap.from({
        ChangeColorAppBar: (dynamic action) => _changeColor(),
      }),
    ).updateState(action, this);
  }

  ColorState _changeColor() {
    if (value) {
      return ColorState(appBarColor: Colors.blue, value: false);
    }
    return ColorState(appBarColor: Colors.yellow, value: true);
  }
}
