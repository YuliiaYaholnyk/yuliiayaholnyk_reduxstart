

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:reduxStart/store/appstate/app_state.dart';
import 'package:reduxStart/store/counter_state/actions.dart';

class ColorSelector {

  static void Function() changeColorAppBar(Store<AppState> store){
    return () => store.dispatch(ChangeColorAppBar());
  }
  static bool getValue(Store<AppState> store){
    return store.state.colorState.value;
  }

  static Color getColor(Store<AppState> store){
    return store.state.colorState.appBarColor;
  }
}